package org.jrt.health_tracker_api.app;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * class to perform integration testting to test the main controller for this application with the rest of the application
 * 
 * @author jrtobac
 *
 */
@RunWith(SpringRunner.class)// allows spring boot to play nicely with JUnit
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Application.class)//Bootstrap the entire container and make an applicationContext for the tests, webenvironment specifies the runtime environment
@AutoConfigureMockMvc//Use this if you need to configure the web layer for testing but don't need to use MockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")//specify the properties file for integration testing
public class MainControllerIntTest {
	
	@Autowired
	private MockMvc mvc;
	
	/**
	 * test that a bp can be returned based on a particular bpid as the input
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetBpById_thenReturnBp() throws Exception {	
		//hits an endpoint and checks the return
		mvc.perform(get("/demo/bps/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.bp_id",  is(1)));
	}
	
	/**
	 * test that all bps are returned when requested
	 * @throws 	Exception
	 */
	@Test
	public void testGetAllBps_thenReturnAllBps() throws Exception {
		mvc.perform(get("/demo/bps").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.[1].bp_id", is(2)));
		
	}
	
	/**
	 * tests that the correct patient can be returned based on a patientid
	 * @throws Exception
	 */
	@Test
	public void testGetPatientByPatientId_thenReturnPatient() throws Exception {
		mvc.perform(get("/demo/patients/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted())
		.andExpect(jsonPath("$.patient_id", is(1)));
	}
	
	/**
	 * test that all patients are returned when requested
	 * @throws	Exception
	 */
	@Test
	public void testGetAllPatients_thenReturnAllPatients() throws Exception {
		mvc.perform(get("/demo/patients").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.[1].patient_id", is(2)));
	}
	
	/**
	 * tests that the correct dr is returned based on a particular drid
	 * @throws Exception
	 */
	@Test
	public void testGetDrByDrId_thenReturnDr() throws Exception {
		
		mvc.perform(get("/demo/drs/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted())
		.andExpect(jsonPath("$.dr_id", is(1)));
	}
	
	/**
	 * test that all drs ge returned when requested
	 * @throws	Exception
	 */
	@Test
	public void testGetAllDrs_thenReturnAllDrs() throws Exception {
		mvc.perform(get("/demo/drs").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.[0].dr_id", is(1)));
	}

}
