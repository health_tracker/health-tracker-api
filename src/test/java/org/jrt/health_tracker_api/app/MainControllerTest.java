package org.jrt.health_tracker_api.app;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.dtos.BpDto;
import org.jrt.health_tracker_api.app.persistence.dtos.DrDto;
import org.jrt.health_tracker_api.app.persistence.dtos.PatientDto;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IBpService;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IDrService;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IPatientService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

/**
 * class to test the main controller for this application
 * 
 * @author jrtobac
 *
 */
@RunWith(SpringRunner.class)// allows spring boot to play nicely with JUnit
@WebMvcTest(MainController.class)//enable spring mvc to be unit tested
public class MainControllerTest {
	
	@Autowired
	private MockMvc mvc;
	
	@MockBean//Creates a mock of a bean
	private IDrService iDrService;

	@MockBean
	private IPatientService iPatientService;
	
	@MockBean
	private IBpService iBpService;
	
	/**
	 * test that a bp can be returned based on a particular bpid as the input
	 * 
	 * @throws Exception
	 */
	@Test
	public void testGetBpById_thenReturnBp() throws Exception {
		BpDto bpDto = new BpDto();
		bpDto.setBp_id(1);
		
		//if a service is given something, state what it will return
		given(iBpService.getBpByBpId(bpDto.getBp_id())).willReturn(new ResponseEntity<>(bpDto, HttpStatus.ACCEPTED));
		
		//hits an endpoint and checks the return
		mvc.perform(get("/demo/bps/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.bp_id",  is(bpDto.getBp_id())));
	}
	
	/**
	 * test that all bps are returned when requested
	 * @throws Exception
	 */
	@Test
	public void testGetAllBps_thenReturnAllBps() throws Exception {
		BpDto bpDtoOne = new BpDto();
		bpDtoOne.setBp_id(1);
		BpDto bpDtoTwo = new BpDto();
		bpDtoTwo.setBp_id(2);
		List<BpDto> allBps = new ArrayList<>();
		allBps.add(bpDtoOne);
		allBps.add(bpDtoTwo);
		
		given(iBpService.getAllBps()).willReturn(new ResponseEntity<>(allBps, HttpStatus.ACCEPTED));
		
		mvc.perform(get("/demo/bps").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.[1].bp_id", is(bpDtoTwo.getBp_id()))).andExpect(jsonPath("$[0].bp_id", is(bpDtoOne.getBp_id())));
	}
	
	/**
	 * tests that the correct patient can be returned based on a patientid
	 * @throws Exception
	 */
	@Test
	public void testGetPatientByPatientId_thenReturnPatient() throws Exception {
		PatientDto patientDto = new PatientDto();
		patientDto.setPatient_id(1);
		
		given(iPatientService.getPatientByPatientId(patientDto.getPatient_id())).willReturn(new ResponseEntity<>(patientDto, HttpStatus.ACCEPTED));
		
		mvc.perform(get("/demo/patients/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted())
		.andExpect(jsonPath("$.patient_id", is(patientDto.getPatient_id())));
	}

	/**
	 * test that all patients get returned when requested
	 * @throws Exception
	 */
	@Test
	public void testGetAllPatients_thenReturnAllPatients() throws Exception {
		PatientDto patientDtoOne = new PatientDto();
		patientDtoOne.setPatient_id(1);
		PatientDto patientDtoTwo = new PatientDto();
		patientDtoTwo.setPatient_id(2);
		List<PatientDto> allPatients = new ArrayList<>();
		allPatients.add(patientDtoOne);
		allPatients.add(patientDtoTwo);
		
		given(iPatientService.getAllPatients()).willReturn(new ResponseEntity<>(allPatients, HttpStatus.ACCEPTED));
		mvc.perform(get("/demo/patients").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.[0].patient_id", is(patientDtoOne.getPatient_id()))).andExpect(jsonPath("$.[1].patient_id", is(patientDtoTwo.getPatient_id())));
	}
	
	
	/**
	 * tests that the correct dr is returned based on a particular drid
	 * @throws Exception
	 */
	@Test
	public void testGetDrByDrId_thenReturnDr() throws Exception {
		
		DrDto drDto = new DrDto();
		drDto.setDr_id(1);
		
		given(iDrService.getDrByDrId(drDto.getDr_id())).willReturn(new ResponseEntity<>(drDto, HttpStatus.ACCEPTED));
		
		mvc.perform(get("/demo/drs/1").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted())
		.andExpect(jsonPath("$.dr_id", is(drDto.getDr_id())));
	}
	
	/**
	 * test that all drs are returned when requested
	 * @throws Exception
	 */
	@Test
	public void testGetAllDrs_thenReturnAllDrs() throws Exception {
		DrDto drDtoOne = new DrDto();
		drDtoOne.setDr_id(1);
		DrDto drDtoTwo = new DrDto();
		drDtoTwo.setDr_id(2);
		List<DrDto> allDrs = new ArrayList<>();
		allDrs.add(drDtoOne);
		allDrs.add(drDtoTwo);
		
		given(iDrService.getAllDrs()).willReturn(new ResponseEntity<>(allDrs, HttpStatus.ACCEPTED));
		
		mvc.perform(get("/demo/drs").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isAccepted()).andExpect(jsonPath("$.[1].dr_id", is(drDtoTwo.getDr_id()))).andExpect(jsonPath("$[0].dr_id", is(drDtoOne.getDr_id())));
	}

}
