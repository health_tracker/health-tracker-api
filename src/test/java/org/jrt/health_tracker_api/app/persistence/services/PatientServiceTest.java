package org.jrt.health_tracker_api.app.persistence.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.jrt.health_tracker_api.app.persistence.daos.IPatientDao;
import org.jrt.health_tracker_api.app.persistence.dtos.PatientDto;
import org.jrt.health_tracker_api.app.persistence.models.Bp;
import org.jrt.health_tracker_api.app.persistence.models.Patient;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IPatientService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * this class is used to test the service layer for the patient entity
 * 
 * @author jrtobac
 *
 */
@RunWith(SpringRunner.class)
public class PatientServiceTest {

	/**
	 * class made for this particular test
	 * tells this test to use the patientservice when searching for the derService interface
	 * 
	 * @author jrtobac
	 *
	 */
	@TestConfiguration
	static class PatientServiceTestContextConfiguration{
		@Bean
		public IPatientService iPatientService() {
			return new PatientService();
		}
	}
	
	@Autowired
	private IPatientService iPatientService;
	
	@MockBean
	private IPatientDao patientDao;	
	
	/**
	 * make sure that the dao returns the correct patient entity
	 */
	@Before
	public void setup() {
		Patient testOne = new Patient();
		Set<Bp> testOneBp = Collections.emptySet();
		testOne.setPatientId(1);
		testOne.setBps(testOneBp);
		
		Patient testTwo = new Patient();
		Set<Bp> testTwoBp = Collections.emptySet();
		testTwo.setPatientId(2);
		testTwo.setBps(testTwoBp);
		
		List<Patient> allPatients = new ArrayList<>();
		allPatients.add(testOne);
		allPatients.add(testTwo);
		
		Mockito.when(patientDao.findByPatientId(testOne.getPatientId())).thenReturn(testOne);
		Mockito.when(patientDao.findAll()).thenReturn(allPatients);
	}
	
	/**
	 * 
	 * tests that the correct patient is returned by the service layer when a particular patientid is given
	 */
	@Test
	public void testGetPatientByPatientId_thenReturnPatient() {
		Integer patientId = 1;
		ResponseEntity<PatientDto> found = iPatientService.getPatientByPatientId(patientId);
		
		assertThat(found.getBody().getPatient_id()).isEqualTo(patientId);
	}

	/**
	 * test that all patients are returned by the service layer when requested
	 */
	@Test
	public void testGetAllPatients() {
		Integer patientIdOne = 1;
		Integer patientIdTwo = 2;
		
		ResponseEntity<List<PatientDto>> found = iPatientService.getAllPatients();
		
		assertThat(found.getBody().get(0).getPatient_id()).isEqualTo(patientIdOne);
		assertThat(found.getBody().get(1).getPatient_id()).isEqualTo(patientIdTwo);
	}

}
