package org.jrt.health_tracker_api.app.persistence.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.daos.IDrDao;
import org.jrt.health_tracker_api.app.persistence.dtos.DrDto;
import org.jrt.health_tracker_api.app.persistence.models.Dr;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IDrService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * this class is used to test the service layer for the dr entity
 * 
 * @author jrtobac
 *
 */
@RunWith(SpringRunner.class)
public class DrServiceTest {
	
	/**
	 * class made for this particular test
	 * tells this test to use the drservice when searching for the derService interface
	 * 
	 * @author jrtobac
	 *
	 */
	@TestConfiguration// used to specify what bean should be picked up during autoscanning
	static class DrServiceTestContextConfiguration{
		@Bean
		public IDrService iDrService() {
			return new DrService();
		}
	}
	
	@Autowired
	private IDrService iDrService;
	
	@MockBean
	private IDrDao drDao;
	
	/**
	 * make sure that the dao returns the correct dr entity
	 */
	@Before
	public void setup() {
		Dr testOne = new Dr();
		testOne.setDrId(1);
		Dr testTwo = new Dr();
		testTwo.setDrId(2);
		List<Dr> allDrs = new ArrayList<>();
		allDrs.add(testOne);
		allDrs.add(testTwo);
		
		Mockito.when(drDao.findByDrId(testOne.getDrId())).thenReturn(testOne);
		Mockito.when(drDao.findAll()).thenReturn(allDrs);
		
	}

	/**
	 * tests that the correct dr is returned by the service layer when a particular drid is given
	 */
	@Test
	public void testGetDrByDrId_thenReturnDr() {
		Integer drId = 1;
		ResponseEntity<DrDto> found = iDrService.getDrByDrId(drId);
		
		assertThat(found.getBody().getDr_id()).isEqualTo(drId);
	}

	/**
	 * test that all drs get returned by the service layer when requested
	 */
	@Test
	public void testGetAllDrs() {
		Integer drIdOne = 1;
		Integer drIdTwo = 2;
		ResponseEntity<List<DrDto>> found = iDrService.getAllDrs();
		
		assertThat(found.getBody().get(0).getDr_id()).isEqualTo(drIdOne);
		assertThat(found.getBody().get(1).getDr_id()).isEqualTo(drIdTwo);
	}

}
