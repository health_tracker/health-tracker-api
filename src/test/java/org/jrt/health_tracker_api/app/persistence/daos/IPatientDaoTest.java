package org.jrt.health_tracker_api.app.persistence.daos;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.models.Patient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * unit test for the patientDao class
 * @author jrtobac
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class IPatientDaoTest {

	@Autowired 
	private TestEntityManager entityManager;
	
	@Autowired
	private IPatientDao patientDao;

	/**
	 * make sure that the correct patient is returned based on the patientId
	 */
	@Test
	public void testFindByPatientId_thenReturnPatient() {
		Patient test = new Patient();
		test.setLName("ltest");
		test.setDob(new Date());
		entityManager.persist(test);
		entityManager.flush();
		
		Patient found = patientDao.findByPatientId(test.getPatientId());
		
		assertThat(found.getPatientId()).isEqualTo(test.getPatientId());
	}
	
	/**
	 * test that all patients are returned when requested
	 */
	@Test
	public void testFindAllPatients_thenReturnAllPatients() {
		Patient testOne = new Patient();
		testOne.setLName("ltestone");
		testOne.setDob(new Date());
		Patient testTwo = new Patient();
		testTwo.setLName("ltesttwo");
		testTwo.setDob(new Date());
		entityManager.persistAndFlush(testOne);
		entityManager.persistAndFlush(testTwo);
		List<Patient> allPatients = new ArrayList<>();
		allPatients.add(testOne);
		allPatients.add(testTwo);
		
		
		List<Patient> foundPatients = patientDao.findAll();
		
		assertThat(foundPatients.get(0).getPatientId()).isEqualTo(allPatients.get(0).getPatientId());
		assertThat(foundPatients.get(1).getPatientId()).isEqualTo(allPatients.get(1).getPatientId());
		
	}

}
