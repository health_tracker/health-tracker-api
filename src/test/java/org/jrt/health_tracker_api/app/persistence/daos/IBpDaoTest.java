package org.jrt.health_tracker_api.app.persistence.daos;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.models.Bp;
import org.jrt.health_tracker_api.app.persistence.models.Dr;
import org.jrt.health_tracker_api.app.persistence.models.Patient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * unit test for the bpDao class
 * 
 * @author jrtobac
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class IBpDaoTest {

	@Autowired
	private TestEntityManager entityManager;
	
	@Autowired
	private IBpDao bpDao;
	
	/**
	 * make sure that the correct bp is returned based on the bpId
	 */
	@Test
	public void testFindByBpId_thenReturnBp() {
		Bp test = new Bp();
		Patient testPatient = new Patient();
		testPatient.setLName("lName");
		testPatient.setDob(new Date());
		entityManager.persistAndFlush(testPatient);
		Dr testDr = new Dr();
		testDr.setLName("lName");
		testDr.setDrOf("drOf");
		entityManager.persistAndFlush(testDr);
		
		test.setTimeTaken(new Date());
		test.setPatient(testPatient);
		test.setDr(testDr);		
		
		entityManager.persistAndFlush(test);
		
		Bp found = bpDao.findByBpId(test.getBpId());
		
		assertThat(found.getBpId()).isEqualTo(test.getBpId());
		
	}
	
	/**
	 * test that all bps are returned when requested
	 */
	public void testFindAllBps_thenReturnAllBps() {
		Bp testOne = new Bp();
		Patient testOnePatient = new Patient();
		testOnePatient.setLName("lNameOne");
		testOnePatient.setDob(new Date());
		entityManager.persistAndFlush(testOnePatient);
		Dr testOneDr = new Dr();
		testOneDr.setLName("lNameOne");
		testOneDr.setDrOf("drOfOne");
		entityManager.persistAndFlush(testOneDr);
		
		testOne.setTimeTaken(new Date());
		testOne.setPatient(testOnePatient);
		testOne.setDr(testOneDr);
		
		Bp testTwo = new Bp();
		Patient testTwoPatient = new Patient();
		testTwoPatient.setLName("lNameTwo");
		testTwoPatient.setDob(new Date());
		entityManager.persistAndFlush(testTwoPatient);
		Dr testTwoDr = new Dr();
		testTwoDr.setLName("lNameTwo");
		testTwoDr.setDrOf("drOfTwo");
		entityManager.persistAndFlush(testTwoDr);
		
		testTwo.setTimeTaken(new Date());
		testTwo.setPatient(testTwoPatient);
		testTwo.setDr(testTwoDr);
		
		entityManager.persistAndFlush(testOne);
		entityManager.persistAndFlush(testTwo);
		
		List<Bp> allBps = new ArrayList<>();
		allBps.add(testOne);
		allBps.add(testTwo);
		
		List<Bp> foundBps = bpDao.findAll();
		
		assertThat(foundBps.get(0).getBpId()).isEqualTo(allBps.get(0).getBpId());
		assertThat(foundBps.get(1).getBpId()).isEqualTo(allBps.get(1).getBpId());
	}
}
