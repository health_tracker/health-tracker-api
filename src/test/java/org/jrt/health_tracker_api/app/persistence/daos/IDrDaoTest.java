package org.jrt.health_tracker_api.app.persistence.daos;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.daos.IDrDao;
import org.jrt.health_tracker_api.app.persistence.models.Dr;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * this class is used to test the dao dr class
 * 
 * @author jrtob
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest//sets up testing for persistence layer
public class IDrDaoTest {

	@Autowired 
	private TestEntityManager entityManager;
	
	@Autowired
	private IDrDao drDao;
	
	/**
	 * tests that the correct dr is returned based on a drid
	 */
	@Test
	public void testFindByDrId_thenReturnDr() {
		Dr test = new Dr();
		test.setLName("ltest");
		test.setDrOf("drOfTest");
		entityManager.persist(test);
		entityManager.flush();
		
		Dr found = drDao.findByDrId(test.getDrId());
		
		assertThat(found.getDrId()).isEqualTo(test.getDrId());
	}
	
	/**
	 * test that all drs are returned when requested
	 */
	@Test
	public void testFindAllDrs_thenReturnAllDrs() {
		Dr testOne = new Dr();
		testOne.setLName("ltestone");
		testOne.setDrOf("drOfTestone");
		Dr testTwo = new Dr();
		testTwo.setLName("ltesttwo");
		testTwo.setDrOf("drOfTesttwo");
		entityManager.persistAndFlush(testOne);
		entityManager.persistAndFlush(testTwo);
		List<Dr> allDrs = new ArrayList<>();
		allDrs.add(testOne);
		allDrs.add(testTwo);
		
		
		List<Dr> foundDrs = drDao.findAll();
		
		assertThat(foundDrs.get(0).getDrId()).isEqualTo(allDrs.get(0).getDrId());
		assertThat(foundDrs.get(1).getDrId()).isEqualTo(allDrs.get(1).getDrId());
	}
	
}
