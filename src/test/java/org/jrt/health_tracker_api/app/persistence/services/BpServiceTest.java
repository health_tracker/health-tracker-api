package org.jrt.health_tracker_api.app.persistence.services;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.daos.IBpDao;
import org.jrt.health_tracker_api.app.persistence.dtos.BpDto;
import org.jrt.health_tracker_api.app.persistence.models.Bp;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IBpService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * this class is used to test the service layer for the bp entity
 * 
 * @author jrtobac
 *
 */
@RunWith(SpringRunner.class)
public class BpServiceTest {

	/**
	 * class made for this particular test
	 * tells this test to use the bpservice when searching for the derService interface
	 * 
	 * @author jrtobac
	 *
	 */
	@TestConfiguration
	static class BpServiceTestContextConfiguration {
		@Bean
		public IBpService iBpService() {
			return new BpService();
		}
	}
	
	@Autowired
	private IBpService iBpService;
	
	@MockBean
	private IBpDao bpDao;

	/**
	 * make sure that the dao returns the correct bp entity
	 */
	@Before
	public void setup() {
		Bp testOne = new Bp();
		testOne.setBpId(1);
		Bp testTwo = new Bp();
		testTwo.setBpId(2);
		
		List<Bp> allBps = new ArrayList<>();
		allBps.add(testOne);
		allBps.add(testTwo);
		
		
		Mockito.when(bpDao.findByBpId(testOne.getBpId())).thenReturn(testOne);
		Mockito.when(bpDao.findAll()).thenReturn(allBps);
	}
	
	/**
	 * tests that the correct bp is returned by the service layer when a particular bpid is given
	 */
	@Test
	public void testGetBpByBpId_thenReturnBp() {
		Integer bpId = 1;
		ResponseEntity<BpDto> found = iBpService.getBpByBpId(bpId);
		
		assertThat(found.getBody().getBp_id()).isEqualTo(bpId);
	}
	
	/**
	 * test that all bps are returned when requested by the service layer
	 */
	@Test
	public void testGetAllBps_thenReturnAllBps() {
		Integer bpIdOne = 1;
		Integer bpIdTwo = 2;
		ResponseEntity<List<BpDto>> found = iBpService.getAllBps();
		
		assertThat(found.getBody().get(0).getBp_id()).isEqualTo(bpIdOne);
		assertThat(found.getBody().get(1).getBp_id()).isEqualTo(bpIdTwo);
	}

}
