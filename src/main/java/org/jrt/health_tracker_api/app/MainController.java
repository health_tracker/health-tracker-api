package org.jrt.health_tracker_api.app;

import java.util.List;

import org.jrt.health_tracker_api.app.persistence.dtos.BpDto;
import org.jrt.health_tracker_api.app.persistence.dtos.DrDto;
import org.jrt.health_tracker_api.app.persistence.dtos.PatientDto;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IBpService;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IDrService;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * this is the main controller for the health tracker api
 * this class is used to handle incoming requests on various urls and return json information based on the requests
 * 
 * @author jrtobac
 *
 */
@Controller //Mark the class as a web MVC controller
@RequestMapping(value="/demo") //Map the value /demo to this class 
public class MainController {
	
	@Autowired//Inject in the bean byType by default
	private IDrService iDrService;//Uses JDK proxies to make the bean
	
	@Autowired
	private IPatientService iPatientService;
	
	@Autowired
	private IBpService iBpService;
	
	/**
	 * returns the json data for all bps
	 * @return	the response entity that contains all the bps
	 */
	@GetMapping(path="/bps")
	public ResponseEntity<List<BpDto>> getAllBps(){
		return iBpService.getAllBps();
	}
	
	/**
	 * returns the JSON data for the bp that the user requested
	 * 
	 * @param bpId 	the id of the particular bp to be returned
	 * @return 		the response entity that contains the information to display the bp requested
	 */
	@GetMapping(path="/bps/{bpId}") //Map the get call to the specified path and make the {} into a variable
	public ResponseEntity<BpDto> getBpByBpId(@PathVariable("bpId") Integer bpId){//Return a ResponseEntity that you built up, and use the pathVariable as an integer
		return iBpService.getBpByBpId(bpId);
	}

	/**
	 * returns the json data for all patients
	 * @return	the response entity that contains all the patients
	 */
	@GetMapping(path="/patients")
	public ResponseEntity<List<PatientDto>> getAllPatients(){
		return iPatientService.getAllPatients();
	}
	
	/**
	 * returns the json data for the patient that the user requested
	 * 
	 * @param patientId	the id of the patient to be returned
	 * @return			the response entity that contains the information to display the patient requested
	 */
	@GetMapping(path="/patients/{patientId}")
	public ResponseEntity<PatientDto> getPatientByPatientId(@PathVariable Integer patientId){
		return iPatientService.getPatientByPatientId(patientId);
	}
	
	/**
	 * returns the json data for all drs
	 * @return	the response entity that contains all the drs
	 */
	@GetMapping(path="/drs")
	public ResponseEntity<List<DrDto>> getAllDrs(){
		return iDrService.getAllDrs();
	}
	
	/**
	 * returns the json data for the dr that the user requested
	 * @param drId	the id of the dr to be returned
	 * @return		the response entity that contains the information to display the dr requested
	 */
	@GetMapping(path="/drs/{drId}")
	public ResponseEntity<DrDto> getDrByDrId(@PathVariable Integer drId){
		return iDrService.getDrByDrId(drId);
	}
}
