package org.jrt.health_tracker_api.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication//set the application to use spring boot
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);//run the application
    }
}