package org.jrt.health_tracker_api.app.persistence.services.interfaces;

import java.util.List;

import org.jrt.health_tracker_api.app.persistence.dtos.PatientDto;
import org.springframework.http.ResponseEntity;

/**
 * interface for the patient service
 * needed when using @transactional on the service layer
 * uses jdk proxies
 * 
 * @author jrtobac
 *
 */
public interface IPatientService {

	ResponseEntity<PatientDto> getPatientByPatientId(Integer patientId);
	
	ResponseEntity<List<PatientDto>> getAllPatients();
}
