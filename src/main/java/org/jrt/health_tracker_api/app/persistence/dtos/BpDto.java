package org.jrt.health_tracker_api.app.persistence.dtos;

import java.util.Date;

/**
 * this class is the data transfer object for the bp entity
 * 
 * @author jrtobac
 *
 */
public class BpDto {
	
	private Integer bp_id;
	
	private Date time_taken;
	
	private Integer systolic;
	
	private Integer diastolic;
	
	private Integer pulse;
	
	private PatientDto patientDto;
	
	private DrDto drDto;

	/**
	 * @return the bp_id
	 */
	public Integer getBp_id() {
		return bp_id;
	}

	/**
	 * @param bp_id the bp_id to set
	 */
	public void setBp_id(Integer bp_id) {
		this.bp_id = bp_id;
	}

	/**
	 * @return the time_taken
	 */
	public Date getTime_taken() {
		return time_taken;
	}

	/**
	 * @param time_taken the time_taken to set
	 */
	public void setTime_taken(Date time_taken) {
		this.time_taken = time_taken;
	}

	/**
	 * @return the systolic
	 */
	public Integer getSystolic() {
		return systolic;
	}

	/**
	 * @param systolic the systolic to set
	 */
	public void setSystolic(Integer systolic) {
		this.systolic = systolic;
	}

	/**
	 * @return the diastolic
	 */
	public Integer getDiastolic() {
		return diastolic;
	}

	/**
	 * @param diastolic the diastolic to set
	 */
	public void setDiastolic(Integer diastolic) {
		this.diastolic = diastolic;
	}

	/**
	 * @return the pulse
	 */
	public Integer getPulse() {
		return pulse;
	}

	/**
	 * @param pulse the pulse to set
	 */
	public void setPulse(Integer pulse) {
		this.pulse = pulse;
	}

	/**
	 * @return the patientDto
	 */
	public PatientDto getPatientDto() {
		return patientDto;
	}

	/**
	 * @param patientDto the patientDto to set
	 */
	public void setPatientDto(PatientDto patientDto) {
		this.patientDto = patientDto;
	}

	/**
	 * @return the drDto
	 */
	public DrDto getDrDto() {
		return drDto;
	}

	/**
	 * @param drDto the drDto to set
	 */
	public void setDrDto(DrDto drDto) {
		this.drDto = drDto;
	}

}
