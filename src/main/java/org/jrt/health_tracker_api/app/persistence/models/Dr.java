package org.jrt.health_tracker_api.app.persistence.models;


import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

/**
 * this class is the model that gets mapped to the actual dr table in the database
 * 
 * @author jrtobac
 *
 */
@Entity//Mark the class as being able to be mapped to a db table
public class Dr {

	@Id//mark this as the primary key id attribute
	@GeneratedValue(strategy=GenerationType.IDENTITY)//specify how the id is generated, in this case IDENTITY is being used so the db can generate the id
	@Column(name="dr_id", nullable=false, unique=true, length=11)//specify the column information for this attribute
	private Integer drId;
	
	@Column(name="f_name", length=50)
	private String fName;
	
	@Column(name="l_name", nullable=false, length=50)
	private String lName;
	
	@Column(name="dr_of", nullable=false, length=255)
	private String drOf;
	
	@OneToMany(mappedBy="dr")//specifies the parent class (one part of the relationship)
	private Set<Bp> bps;

	/**
	 * @return the drId
	 */
	public Integer getDrId() {
		return drId;
	}

	/**
	 * @param drId the drId to set
	 */
	public void setDrId(Integer drId) {
		this.drId = drId;
	}

	/**
	 * @return the fName
	 */
	public String getFName() {
		return fName;
	}

	/**
	 * @param fName the fName to set
	 */
	public void setFName(String fName) {
		this.fName = fName;
	}

	/**
	 * @return the lName
	 */
	public String getLName() {
		return lName;
	}

	/**
	 * @param lName the lName to set
	 */
	public void setLName(String lName) {
		this.lName = lName;
	}

	/**
	 * @return the drOf
	 */
	public String getDrOf() {
		return drOf;
	}

	/**
	 * @param drOf the drOf to set
	 */
	public void setDrOf(String drOf) {
		this.drOf = drOf;
	}

	/**
	 * @return the bps
	 */
	public Set<Bp> getBps() {
		return bps;
	}

	/**
	 * @param bps the bps to set
	 */
	public void setBps(Set<Bp> bps) {
		this.bps = bps;
	}
	
}
