package org.jrt.health_tracker_api.app.persistence.services;

import java.util.ArrayList;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.daos.IBpDao;
import org.jrt.health_tracker_api.app.persistence.dtos.BpDto;
import org.jrt.health_tracker_api.app.persistence.dtos.DrDto;
import org.jrt.health_tracker_api.app.persistence.dtos.PatientDto;
import org.jrt.health_tracker_api.app.persistence.models.Bp;
import org.jrt.health_tracker_api.app.persistence.models.Dr;
import org.jrt.health_tracker_api.app.persistence.models.Patient;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IBpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * service layer class for the bp entities
 * used to perform business logic on the bps
 * 
 * @author jrtobac
 *
 */
@Service
@Transactional
public class BpService implements IBpService{

	@Autowired
	private IBpDao bpDao;
	
	/**
	 * get a specific bp based on a given bpId
	 * include linked patients and drs
	 * 
	 * @param bpId		bpId of the bp that is to be returned
	 * @return			bp for the given bpId
	 */
	public ResponseEntity<BpDto> getBpByBpId(Integer bpId){
		Bp bp = bpDao.findByBpId(bpId);
		Patient patient = bp.getPatient();
		Dr dr = bp.getDr();
		BpDto bpDto = new BpDto();
		
		bpDto.setBp_id(bp.getBpId());
		bpDto.setTime_taken(bp.getTimeTaken());
		bpDto.setSystolic(bp.getSystolic());
		bpDto.setDiastolic(bp.getDiastolic());
		bpDto.setPulse(bp.getPulse());
		if(patient != null) {
			PatientDto patientDto = new PatientDto();
			patientDto.setPatient_id(patient.getPatientId());
			patientDto.setF_name(patient.getFName());
			patientDto.setL_name(patient.getLName());
			patientDto.setDob(patient.getDob());
			bpDto.setPatientDto(patientDto);
		}
		
		if(dr != null) {
			DrDto drDto = new DrDto();
			drDto.setDr_id(dr.getDrId());
			drDto.setF_name(dr.getFName());
			drDto.setL_name(dr.getLName());
			drDto.setDr_of(dr.getDrOf());
			bpDto.setDrDto(drDto);
		}
		return new ResponseEntity<>(bpDto, HttpStatus.ACCEPTED);
		
	}
	
	/**
	 * get all bps
	 * include linked patients and drs
	 * 
	 * @return		all bps
	 */
	public ResponseEntity<List<BpDto>> getAllBps() {
		List<Bp> bps = bpDao.findAll();
		List<BpDto> bpDtos = new ArrayList<>();
		
		for(Bp bp : bps) {
			BpDto bpDto = new BpDto();
			Patient patient = bp.getPatient();
			Dr dr = bp.getDr();
			
			bpDto.setBp_id(bp.getBpId());
			bpDto.setTime_taken(bp.getTimeTaken());
			bpDto.setSystolic(bp.getSystolic());
			bpDto.setDiastolic(bp.getDiastolic());
			bpDto.setPulse(bp.getPulse());
			
			if(patient != null) {
				PatientDto patientDto = new PatientDto();
				patientDto.setPatient_id(patient.getPatientId());
				patientDto.setF_name(patient.getFName());
				patientDto.setL_name(patient.getLName());
				patientDto.setDob(patient.getDob());
				bpDto.setPatientDto(patientDto);
			}
			
			if(dr != null) {
				DrDto drDto = new DrDto();
				drDto.setDr_id(dr.getDrId());
				drDto.setF_name(dr.getFName());
				drDto.setL_name(dr.getLName());
				drDto.setDr_of(dr.getDrOf());
				bpDto.setDrDto(drDto);
			}
			
			bpDtos.add(bpDto);
		}		
		
		return new ResponseEntity<>(bpDtos, HttpStatus.ACCEPTED);
	}
}
