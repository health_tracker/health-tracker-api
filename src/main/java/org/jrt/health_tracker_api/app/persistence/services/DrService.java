package org.jrt.health_tracker_api.app.persistence.services;


import java.util.ArrayList;
import java.util.List;

import org.jrt.health_tracker_api.app.persistence.daos.IDrDao;
import org.jrt.health_tracker_api.app.persistence.dtos.DrDto;
import org.jrt.health_tracker_api.app.persistence.models.Dr;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IDrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * service layer class for the dr entities
 * used to perform business logic on the drs
 * 
 * @author jrtobac
 *
 */
@Service//Same as @component so the bean is set by default as a singleton
@Transactional//Set the class as transactional to ensure entire business logic is committed or rolled back
public class DrService implements IDrService{

	@Autowired
	private IDrDao drDao;
	
	/**
	 * gets the dr based on the incoming id
	 * fills out the dto with all the needed information
	 * returns the responseEntity with the dto
	 * 
	 * @param drId	id of the dr to return
	 * @return 		the responseEntity filled out with the dto
	 * 
	 */
	public ResponseEntity<DrDto> getDrByDrId(Integer drId){
		Dr dr = drDao.findByDrId(drId);
		
		DrDto drDto = new DrDto();
		drDto.setDr_id(dr.getDrId());
		drDto.setF_name(dr.getFName());
		drDto.setL_name(dr.getLName());
		drDto.setDr_of(dr.getDrOf());
		
		return new ResponseEntity<>(drDto, HttpStatus.ACCEPTED);
	}
	
	/**
	 * gets all the doctors that exist in the db
	 * 
	 * @return 		the responseEntity containing all of the drDtos
	 */
	public ResponseEntity<List<DrDto>> getAllDrs() {
		List<Dr> drs = drDao.findAll();
		List<DrDto> drDtos = new ArrayList<>();
		
		for(Dr dr : drs) {
			DrDto drDto = new DrDto();
			drDto.setDr_id(dr.getDrId());
			drDto.setF_name(dr.getFName());
			drDto.setL_name(dr.getLName());
			drDto.setDr_of(dr.getDrOf());
			drDtos.add(drDto);
		}		
		
		return new ResponseEntity<>(drDtos, HttpStatus.ACCEPTED);
	}
}
