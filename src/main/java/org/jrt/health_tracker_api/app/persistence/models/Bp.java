package org.jrt.health_tracker_api.app.persistence.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * this class is the model that gets mapped to the actual bp table in the database
 * 
 * @author jrtobac
 *
 */
@Entity
public class Bp {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="bp_id", unique=true, nullable=false, length=11)
	private Integer bpId;
	
	@Column(name="time_taken", nullable=false)
	private Date timeTaken;
	
	@Column(name="systolic", length=11)
	private Integer systolic;
	
	@Column(name="diastolic", length=11)
	private Integer diastolic;
	
	@Column(name="pulse", length=11)
	private Integer pulse;
	
	@ManyToOne
	@JoinColumn(name="patient_id", nullable=false)
	private Patient patient;
	
	@ManyToOne//Specifies many to one relationship
	@JoinColumn(name="dr_id", nullable=false)//specifies what column the many entity joins to the one entity
	private Dr dr;
	
	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * @return the dr
	 */
	public Dr getDr() {
		return dr;
	}

	/**
	 * @param dr the dr to set
	 */
	public void setDr(Dr dr) {
		this.dr = dr;
	}

	/**
	 * @return the bpId
	 */
	public Integer getBpId() {
		return bpId;
	}

	/**
	 * @param bpId the bpId to set
	 */
	public void setBpId(Integer bpId) {
		this.bpId = bpId;
	}

	/**
	 * @return the timeTaken
	 */
	public Date getTimeTaken() {
		return timeTaken;
	}

	/**
	 * @param timeTaken the timeTaken to set
	 */
	public void setTimeTaken(Date timeTaken) {
		this.timeTaken = timeTaken;
	}

	/**
	 * @return the systolic
	 */
	public Integer getSystolic() {
		return systolic;
	}

	/**
	 * @param systolic the systolic to set
	 */
	public void setSystolic(Integer systolic) {
		this.systolic = systolic;
	}

	/**
	 * @return the diastolic
	 */
	public Integer getDiastolic() {
		return diastolic;
	}

	/**
	 * @param diastolic the diastolic to set
	 */
	public void setDiastolic(Integer diastolic) {
		this.diastolic = diastolic;
	}

	/**
	 * @return the pulse
	 */
	public Integer getPulse() {
		return pulse;
	}

	/**
	 * @param pulse the pulse to set
	 */
	public void setPulse(Integer pulse) {
		this.pulse = pulse;
	}

}
