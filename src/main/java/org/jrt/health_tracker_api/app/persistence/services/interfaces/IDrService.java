package org.jrt.health_tracker_api.app.persistence.services.interfaces;

import java.util.List;

import org.jrt.health_tracker_api.app.persistence.dtos.DrDto;
import org.springframework.http.ResponseEntity;

/**
 * interface for the dr service
 * needed when using @transactional on the service layer
 * uses jdk proxies
 * 
 * @author jrtobac
 *
 */
public interface IDrService {

	ResponseEntity<DrDto> getDrByDrId(Integer drId);
	
	ResponseEntity<List<DrDto>> getAllDrs();
}
