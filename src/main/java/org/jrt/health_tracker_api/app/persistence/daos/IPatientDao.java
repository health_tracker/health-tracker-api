package org.jrt.health_tracker_api.app.persistence.daos;

import org.jrt.health_tracker_api.app.persistence.models.Patient;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * this interface is used to create the patientDAO for hibernate to implement
 * 
 * @author jrtobac
 *
 */
public interface IPatientDao extends JpaRepository<Patient, Integer>{
	Patient findByPatientId(Integer patientId);
}
