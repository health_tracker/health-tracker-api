package org.jrt.health_tracker_api.app.persistence.daos;

import org.jrt.health_tracker_api.app.persistence.models.Dr;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * this interface is used to create the drDAO for hibernate to implement
 * 
 * @author jrtobac
 *
 */
public interface IDrDao extends JpaRepository<Dr, Integer>{
	/**
	 * find the dr based on the dr id
	 * 
	 * @param drId	id of the dr entity to be returned
	 * @return		dr entity 
	 */
	Dr findByDrId(Integer drId);
}
