package org.jrt.health_tracker_api.app.persistence.dtos;

import java.util.Date;
import java.util.Set;

/**
 * this class is the data transfer object for the patient entity
 * 
 * @author jrtobac
 *
 */
public class PatientDto {

	private Integer patient_id;
	
	private String f_name;
	
	private String l_name;
	
	private Date dob;
	
	private Set<BpDto> bps;

	/**
	 * @return the patient_id
	 */
	public Integer getPatient_id() {
		return patient_id;
	}

	/**
	 * @param patient_id the patient_id to set
	 */
	public void setPatient_id(Integer patient_id) {
		this.patient_id = patient_id;
	}

	/**
	 * @return the f_name
	 */
	public String getF_name() {
		return f_name;
	}

	/**
	 * @param f_name the f_name to set
	 */
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	/**
	 * @return the l_name
	 */
	public String getL_name() {
		return l_name;
	}

	/**
	 * @param l_name the l_name to set
	 */
	public void setL_name(String l_name) {
		this.l_name = l_name;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the bps
	 */
	public Set<BpDto> getBps() {
		return bps;
	}

	/**
	 * @param bps the bps to set
	 */
	public void setBps(Set<BpDto> bps) {
		this.bps = bps;
	}
	
	
}
