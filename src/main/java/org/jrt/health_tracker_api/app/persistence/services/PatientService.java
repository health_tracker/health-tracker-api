package org.jrt.health_tracker_api.app.persistence.services;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.jrt.health_tracker_api.app.persistence.daos.IPatientDao;
import org.jrt.health_tracker_api.app.persistence.dtos.BpDto;
import org.jrt.health_tracker_api.app.persistence.dtos.PatientDto;
import org.jrt.health_tracker_api.app.persistence.models.Bp;
import org.jrt.health_tracker_api.app.persistence.models.Patient;
import org.jrt.health_tracker_api.app.persistence.services.interfaces.IPatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * service layer class for the patient entities
 * used to perform business logic on the patients
 * 
 * @author jrtobac
 *
 */
@Service
@Transactional
public class PatientService implements IPatientService{

	@Autowired
	private IPatientDao patientDao;
	
	/**
	 * get a specific patient based on a patientId
	 * 
	 * @param patientId		specific patientid of the patient that is to be returned
	 * @return				specific patient based on the patientId
	 */
	public ResponseEntity<PatientDto> getPatientByPatientId(Integer patientId){
		Patient patient = patientDao.findByPatientId(patientId);
		PatientDto patientDto = new PatientDto();
		
		Set<BpDto> bpDtos = new HashSet<BpDto>();
		patientDto.setPatient_id(patient.getPatientId());
		patientDto.setF_name(patient.getFName());
		patientDto.setL_name(patient.getLName());
		patientDto.setDob(patient.getDob());
		for(Bp bp: patient.getBps()) {
			BpDto bpDto = new BpDto();
			bpDto.setBp_id(bp.getBpId());
			bpDto.setTime_taken(bp.getTimeTaken());
			bpDto.setSystolic(bp.getSystolic());
			bpDto.setDiastolic(bp.getDiastolic());
			bpDto.setPulse(bp.getPulse());
			bpDtos.add(bpDto);
		}
		patientDto.setBps(bpDtos);
		
		return new ResponseEntity<>(patientDto, HttpStatus.ACCEPTED);
		
	}
	
	/**
	 * get all patients
	 * 
	 * @return		all patients
	 */
	public ResponseEntity<List<PatientDto>> getAllPatients() {
		List<Patient> patients = patientDao.findAll();
		List<PatientDto> patientDtos = new ArrayList<>();
		
		for(Patient patient: patients) {
			PatientDto patientDto = new PatientDto();
			Set<BpDto> bpDtos = new HashSet<BpDto>();
			patientDto.setPatient_id(patient.getPatientId());
			patientDto.setF_name(patient.getFName());
			patientDto.setL_name(patient.getLName());
			patientDto.setDob(patient.getDob());
			for(Bp bp: patient.getBps()) {
				BpDto bpDto = new BpDto();
				bpDto.setBp_id(bp.getBpId());
				bpDto.setTime_taken(bp.getTimeTaken());
				bpDto.setSystolic(bp.getSystolic());
				bpDto.setDiastolic(bp.getDiastolic());
				bpDto.setPulse(bp.getPulse());
				bpDtos.add(bpDto);
			}
			patientDto.setBps(bpDtos);
			patientDtos.add(patientDto);
		}
		
		return new ResponseEntity<>(patientDtos, HttpStatus.ACCEPTED);
	}
}
