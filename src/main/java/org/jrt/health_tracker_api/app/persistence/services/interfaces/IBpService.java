package org.jrt.health_tracker_api.app.persistence.services.interfaces;

import java.util.List;

import org.jrt.health_tracker_api.app.persistence.dtos.BpDto;
import org.springframework.http.ResponseEntity;

/**
 * interface for the bp service
 * needed when using @transactional on the service layer
 * uses jdk proxies
 * 
 * @author jrtobac
 *
 */
public interface IBpService {

	ResponseEntity<BpDto> getBpByBpId(Integer bpId);
	
	ResponseEntity<List<BpDto>> getAllBps();
}
