package org.jrt.health_tracker_api.app.persistence.daos;

import org.jrt.health_tracker_api.app.persistence.models.Bp;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * this interface is used to create the bpDAO for hibernate to implement
 * 
 * @author jrtobac
 *
 */
public interface IBpDao extends JpaRepository<Bp, Integer> {
	Bp findByBpId(Integer bpId);
}
