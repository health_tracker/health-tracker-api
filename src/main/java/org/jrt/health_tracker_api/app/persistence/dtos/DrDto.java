package org.jrt.health_tracker_api.app.persistence.dtos;
/**
 * this class is the data transfer object for the dr entity
 * 
 * @author jrtobac
 *
 */
public class DrDto {
	
	private Integer dr_id;
	
	private String f_name;
	
	private String l_name;
	
	private String dr_of;

	/**
	 * @return the dr_id
	 */
	public Integer getDr_id() {
		return dr_id;
	}

	/**
	 * @param dr_id the dr_id to set
	 */
	public void setDr_id(Integer dr_id) {
		this.dr_id = dr_id;
	}

	/**
	 * @return the f_name
	 */
	public String getF_name() {
		return f_name;
	}

	/**
	 * @param f_name the f_name to set
	 */
	public void setF_name(String f_name) {
		this.f_name = f_name;
	}

	/**
	 * @return the l_name
	 */
	public String getL_name() {
		return l_name;
	}

	/**
	 * @param l_name the l_name to set
	 */
	public void setL_name(String l_name) {
		this.l_name = l_name;
	}

	/**
	 * @return the dr_of
	 */
	public String getDr_of() {
		return dr_of;
	}

	/**
	 * @param dr_of the dr_of to set
	 */
	public void setDr_of(String dr_of) {
		this.dr_of = dr_of;
	}
	
	
}
