package org.jrt.health_tracker_api.app.persistence.models;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * this class is the model that gets mapped to the actual patient table in the database
 * 
 * @author jrtobac
 *
 */
@Entity
public class Patient {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)//TODO Change to SEQUENCE
	@Column(name="patient_id", nullable=false, length=11, unique=true)
	private Integer patientId;
	
	@Column(name="f_name", length=50)
	private String fName;
	
	@Column(name="l_name", nullable=false, length=50)
	private String lName;
	
	@Temporal(TemporalType.DATE)
	@Column(name="dob", nullable=false)
	private Date dob;
	
	@OneToMany(mappedBy="patient")
	private Set<Bp> bps;

	/**
	 * @return the patientId
	 */
	public Integer getPatientId() {
		return patientId;
	}

	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(Integer patientId) {
		this.patientId = patientId;
	}

	/**
	 * @return the fName
	 */
	public String getFName() {
		return fName;
	}

	/**
	 * @param fName the fName to set
	 */
	public void setFName(String fName) {
		this.fName = fName;
	}

	/**
	 * @return the lName
	 */
	public String getLName() {
		return lName;
	}

	/**
	 * @param lName the lName to set
	 */
	public void setLName(String lName) {
		this.lName = lName;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the bps
	 */
	public Set<Bp> getBps() {
		return bps;
	}

	/**
	 * @param bps the bps to set
	 */
	public void setBps(Set<Bp> bps) {
		this.bps = bps;
	} 
	
	
}
